import { shallowMount } from "@vue/test-utils";
import Welcome from "@/stories/templates/Welcome";

describe("Welcome.vue", () => {
  it("renders props.title when passed", () => {
    const expected = "new message";
    const wrapper = shallowMount(Welcome, {
      propsData: { title: expected }
    });
    expect(wrapper.text()).toMatch(expected);
  });
});
