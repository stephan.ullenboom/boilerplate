module.exports = {
  root: true,

  env: {
    node: true
  },

  extends: ["plugin:vue/recommended", "@vue/prettier"],

  rules: {
    "no-console": "off",
    "no-debugger": "off",
    "vue/html-closing-bracket-newline": "warning",
    "vue/html-closing-bracket-spacing": "warning",
    "vue/no-use-v-if-with-v-for": "warning",
    "vue/prop-name-casing": "warning",
    "vue/script-indent": "warning"
  },

  parserOptions: {
    parser: "babel-eslint"
  }
};
