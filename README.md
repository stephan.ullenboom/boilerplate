# Sika frontend

### Setup
```
yarn install

# If `vue-cli-service` are missing then you should update the Vue cli to version 3
yarn global add @vue/cli
```

### Deploying
```
# Compiles and hot-reloads for development
yarn run start

# Compiles and hot-reloads for development with the storybook
yarn run start:storybook
```

### Testing and linting files
```
# Lints and fixes files
yarn run lint

# Runs `lint`,`test:unit` and `test:e2e`
yarn run test:unit

# Run your unit tests
yarn run test:unit

# Run your end-to-end tests
yarn run test:e2e
```

### Build Setup and deploy
```
# Compiles and minifies for production
yarn run build

# Compiles and minifies the storybook for production
yarn run build:storybook

# Build and deploy the fronend bundle
yarn run deploy
```
