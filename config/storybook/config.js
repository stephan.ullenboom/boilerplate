/* eslint-disable import/no-extraneous-dependencies */
import { configure } from "@storybook/vue";
import { setOptions } from "@storybook/addon-options";

// Import the application styling
import "@/assets/css/main.scss";

// Import all registered components
import "@/register/components";

/**
 * load all stories from the app
 */
function loadStories() {
  require("../../src/stories/index");

  // automatically import all files ending or includes *.stories
  const req = require.context("../../src/stories", true, /\.stories/);
  req.keys().forEach(filename => req(filename));
}

setOptions({
  name: "SIKA Storybook",
  hierarchyRootSeparator: /\|/
});

configure(loadStories, module);
