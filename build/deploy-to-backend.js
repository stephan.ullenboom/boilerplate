const chalk = require("chalk");
const fs = require("fs-extra");
const source = "dist";
const dest =
  "../ui.apps/src/main/content/jcr_root/apps/sika/clientlibs/clientlib-base/";
const assetsDest = "assets";

const options = {
  filter: function(path) {
    if (
      path.indexOf(".md") !== -1 ||
      path.indexOf(".html") !== -1 ||
      path.indexOf(".json") !== -1
    ) {
      return false;
    }
    return true;
  }
};

fs.remove(dest + assetsDest)
  .then(() => {
    fs.copy(source, dest, options, err => {
      if (err) {
        return console.error(err);
      }
      console.log(
        chalk.cyan(
          "Copied generated files to backend project in: " + dest + "\n"
        )
      );
    });
  })
  .catch(err => {
    return console.error(err);
  });
