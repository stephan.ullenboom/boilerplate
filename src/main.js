/** ============
 * Main File
 * =============
 *
 * Will initialize the application.
 */
import Vue from "vue";

// Import the application styling
import "./assets/css/main.scss";

// Import all registered components
import "./register/components";

// Import the main application.
import App from "./App";

Vue.config.productionTip = process.env.NODE_ENV === "production";

new Vue({
  render: h => h(App)
}).$mount("#app");
