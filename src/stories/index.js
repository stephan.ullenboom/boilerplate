import { storiesOf } from "@storybook/vue";
import Welcome from "./templates/Welcome";
import Colors from "./templates/Colors";
import Typography from "./templates/Typography";

storiesOf("Welcome", module).add("Welcome", () => ({
  render: h => h(Welcome)
}));

storiesOf("Basic|Colors", module).add("Basic colors", () => ({
  components: { Colors },
  template: `<colors />`
}));
storiesOf("Basic|Typography", module).add("Headings", () => ({
  components: { Typography },
  template: `<typography />`
}));
