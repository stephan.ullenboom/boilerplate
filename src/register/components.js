/* ============
 * Register File
 * ============
 *
 * Will register all components for AEM
 * - That we need for creating the css bundle with the component css
 */
import Vue from "vue";

import Input from "../components/Input";
import Button from "../components/Button";
import ContactForm from "../components/ContactForm";

// Register the components to Vue
Vue.use(Input);
Vue.use(Button);
Vue.use(ContactForm);
